package com.voxloud.provisioning.util;

public final class Constants {

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String DOMAIN = "domain";
    public static final String PORT = "port";
    public static final String CODECS = "codecs";
    public static final String TIMEOUT = "timeout";

}
