package com.voxloud.provisioning.service;

import org.springframework.http.ResponseEntity;

public interface ProvisioningService {

    ResponseEntity<Object> getProvisioningFile(String macAddress);
}
