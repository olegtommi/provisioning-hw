package com.voxloud.provisioning.service;

import com.google.common.base.Splitter;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static com.voxloud.provisioning.entity.Device.DeviceModel.CONFERENCE;
import static com.voxloud.provisioning.entity.Device.DeviceModel.DESK;
import static com.voxloud.provisioning.util.Constants.*;

@Service
@Transactional
public class ProvisioningServiceImpl implements ProvisioningService {

    @Value("${provisioning.domain}")
    private String domain;

    @Value("${provisioning.port}")
    private String port;

    @Value("${provisioning.codecs}")
    private String codecs;

    private String timeout;

    @Autowired
    private DeviceRepository deviceRepository;

    public ResponseEntity<Object> getProvisioningFile(String macAddress) {
        Device device = deviceRepository.findById(macAddress).orElse(null);

        if (device == null) {
            return ResponseEntity.notFound().build();
        }

        if (device.getModel().equals(CONFERENCE)) {
            HashMap<String, Object> map = new HashMap<>();

            map.put(USERNAME, device.getUsername());
            map.put(PASSWORD, device.getPassword());
            map.put(CODECS, Collections.singletonList(codecs));

            if (StringUtils.isEmpty(device.getOverrideFragment())) {
                map.put(DOMAIN, domain);
                map.put(PORT, port);
            } else {
                convertStringToMapConference(device.getOverrideFragment()).forEach((key, value) -> {
                    key = key.replace("}", "").replace("{", "").replace("\"", "");
                    value = value.replace("}", "").replace("{", "").replace("\"", "");

                    if (key.equals(DOMAIN)) {
                        map.put(DOMAIN, value);
                    }

                    if (key.equals(PORT)) {
                        map.put(PORT, value);
                    }

                    if (key.equals(TIMEOUT)) {
                        map.put(TIMEOUT, Integer.parseInt(value));
                    }
                });
            }

            return ResponseEntity.ok(map);
        }

        if (device.getModel().equals(DESK)) {
            String codecs = CODECS + "=" + this.codecs;
            String username = USERNAME + "=" + device.getUsername();
            String password = PASSWORD + "=" + device.getPassword();

            byte [] timeoutToBytes = null;
            File file = null;
            FileOutputStream outputStream = null;
            UrlResource resource = null;

            if (StringUtils.isEmpty(device.getOverrideFragment())) {
                domain = DOMAIN + "=" + this.domain;
                port = PORT + "=" + this.port;
            } else {
                convertStringToMapDesk(device.getOverrideFragment()).forEach((key, value) -> {
                    if (key.equals(DOMAIN)) {
                        domain = DOMAIN + "=" + value;
                    }

                    if (key.equals(PORT)) {
                        port = PORT + "=" + value;
                    }

                    if (key.equals(TIMEOUT)) {
                        timeout = TIMEOUT + "=" + value;
                    }
                });
            }

            try {
                outputStream = new FileOutputStream("Property_file_" + device.getMacAddress());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            byte[] usernameToBytes = username.getBytes();
            byte[] passwordToBytes = password.getBytes();
            byte[] domainToBytes = domain.getBytes();
            byte[] portToBytes = port.getBytes();
            byte[] codecsToBytes = codecs.getBytes();

            if (!StringUtils.isEmpty(timeout)) {
                timeoutToBytes = timeout.getBytes();
            }

            try {
                if (outputStream != null) {
                    outputStream.write(usernameToBytes);
                    outputStream.write('\n');
                    outputStream.write(passwordToBytes);
                    outputStream.write('\n');
                    outputStream.write(domainToBytes);
                    outputStream.write('\n');
                    outputStream.write(portToBytes);
                    outputStream.write('\n');
                    outputStream.write(codecsToBytes);
                    outputStream.write('\n');

                    if (timeoutToBytes != null) {
                        outputStream.write(timeoutToBytes);
                        outputStream.write('\n');
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                file = getStoredFileByMacAddress(device.getMacAddress());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            if (StringUtils.isEmpty(file)) {
                return ResponseEntity.notFound().build();
            }

            try {
                resource = loadFileAsResource(file.getName(), device.getMacAddress());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                    .body(resource);
        }

        return ResponseEntity.badRequest().build();
    }

    private UrlResource loadFileAsResource(String fileName, String macAddress) throws Exception {
        try {
            Path filePath = Paths.get(getStoredFileByMacAddress(macAddress).getPath());
            UrlResource resource = new UrlResource(filePath.toUri());

            if(!StringUtils.isEmpty(resource)) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName);
        }
    }

    private File getStoredFileByMacAddress(String macAddress) throws FileNotFoundException {
        File file = ResourceUtils.getFile("C:/Users/User/Desktop/provisioning-hw/Property_file_" + macAddress);

        if (StringUtils.isEmpty(file)) {
            throw new FileNotFoundException();
        }

        return file;
    }

    public Map<String, String> convertStringToMapConference(String mapAsString) {
        return Splitter.on(',').withKeyValueSeparator(':').split(mapAsString);
    }

    public Map<String, String> convertStringToMapDesk(String mapAsString) {
        return Splitter.on('\n').withKeyValueSeparator('=').split(mapAsString);
    }
}
